# README

This is template for setting up a project structure for creating presentations using [Markdown](http://daringfireball.net/projects/markdown/).

* The presentation is styled using CSS.
* It's generated into HTML using [Jekyll](http://jekyllrb.com).
* The slides are generated using [RemarkJS](https://remarkjs.com/).
* Via Gulp and BrowserSync it is possible to interactively write the presentation and see the resulting slides in the browser.

# Getting started

* Clone this project.
* Run 'gulp' in the root of the project.  
  This will start [Jekyll](http://jekyllrb.com) in watch-mode and a server with browser-sync and it will open the generated slides in the browser.

This shows a sample presentation.  
The markdown for this presentation is in 'index.html'.  
Jekyll will generate a HTML version in the _site folder using a layout which is defined in the markdown file.  
The layout file is located in the _layout folder.  
It will use a css-style based on the 'conference' and 'year' variables in the markdown file. The css file is located in the 'remarkjs/css' folder.

Additional images can be put in the 'images' folder.

A layout for the **Devoxx 2016** conference is available since I was creating this for a presentation I present there.

### node_modules
The node_modules folder is checked in on purpose to make this a clone-and-play repo. I have had so many problems in the past with Node/NPM/Gulp etc because of updated or deleted dependencies and I just want to be able to use this everywhere.

# Generating PDF

Using 'decktape' the RemarkJS slides can be generated into a pdf.

Use command:
decktape-1.0.0/phantomjs decktape-1.0.0/decktape.js remark http://localhost:3000/ slides.pdf

See https://github.com/astefanutti/decktape for installation.